#!/usr/bin/env python3

import contextlib
import logging
import os
import os.path

import ccf_xfce
import conans

available_versions = [
    "4.14.2",
    "4.13.6",
]


class TheConan(ccf_xfce.ConanFile):
    default_user = "aucampia"
    default_channel = "default"
    name = "xfdesktop"
    license = "GPL-2.0-only"
    url = "https://www.xfce.org/"
    exports = "ccf_xfce.py"
    generators = ("pkg_config", "txt", "json")
    version = [v for v in available_versions if "-" not in v][0]
    settings = {"os": ["Linux"], "arch": ["x86_64"]}
    build_policy = "missing"
    no_copy_source = True
    options = {
        "version": available_versions,
        "source_type": ["archive", "scm"],
        "shared": [True, False],
        "fPIC": [True, False],
    }
    default_options = {
        "version": [v for v in available_versions if "-" not in v][0],
        "source_type": "archive",
        "shared": True,
        "fPIC": True,
    }

    @ccf_xfce.mextends_after
    def __init__(self, *args, **kwargs):
        logging.info("entry ...")
        self._xfce_version = "4.13"

    def requirements(self):
        logging.info("entry: ...")
        self.requires_ws(("garcon/[^0.6]"))
        self.requires_ws(("exo/[^0.12]"))
        self.requires_ws(("libxfce4util/[^{}]").format(self.xfce_version))
        self.requires_ws(("libxfce4ui/[^{}]").format(self.xfce_version))
        self.requires_ws(("xfconf/[^{}]").format(self.xfce_version))
        self.requires_ws(("thunar/[^1.8]"))
        logging.info("self.requires = %s", self.requires)

    @ccf_xfce.mextends_after
    def system_requirements(self):
        logging.info("entry: ...")
        os_info = conans.tools.os_info
        installer = conans.tools.SystemPackageTool()
        packages = []
        if os_info.linux_distro in ("rhel", "fedora", "centos"):
            # run
            packages.append("libnotify")
            packages.append("libwnck3")
            packages.append("cairo")
            # build
            packages.append("make")
            packages.append("gcc")
            packages.append("pkgconf-pkg-config")
            packages.append("intltool")
            packages.append("libnotify-devel")
            packages.append("libwnck3-devel")
            packages.append("cairo-devel")
        logging.info("packages = %s", packages)
        if packages:
            for package in packages:
                installer.install(package)

    def build(self):
        logging.info("entry ...")
        # conans.tools.unzip(pathlib.Path(self.source_folder) / self._source_file)
        source_path = os.path.join(self.source_folder, self._source_file)
        logging.info("source_path = %s", source_path)
        conans.tools.unzip(source_path)
        # https://docs.conan.io/en/latest/integrations/build_system/pkg_config_pc_files.html
        xenv = {
            "XDG_DATA_DIRS": [
                *(
                    os.environ["XDG_DATA_DIRS"].split(os.pathsep)
                    if "XDG_DATA_DIRS" in os.environ
                    else []
                ),
                "/usr/local/share",
                "/usr/share",
            ],
        }
        logging.info("xenv = %s", xenv)
        with contextlib.ExitStack() as stack:
            stack.enter_context(conans.tools.chdir(self._source_dirname))
            stack.enter_context(conans.tools.environment_append(xenv))
            autotools = conans.AutoToolsBuildEnvironment(self)
            logging.info("autotools.vars = %s", autotools.vars)
            configure_args = []
            configure_args.append("--enable-notifications")
            configure_args.append("--enable-file-icons")
            configure_args.append("--enable-thunarx")
            autotools.configure(args=configure_args)
            autotools.make(args=["VERBOSE=1", "AM_DEFAULT_VERBOSITY=1", "V=1"])


def direct_main():
    pass


if __name__ == "__main__":
    direct_main()
