# vim: set ft=python sts=4 ts=4 sw=4 expandtab fo-=t:
import json
import logging
import os
import sys

# from conans import ConanFile
# from conans.model import Generator
import conans

logging.basicConfig(
    level=logging.INFO,
    datefmt="%Y-%m-%dT%H:%M:%S",
    stream=sys.stderr,
    format="%(asctime)s %(process)d %(thread)d"
    "%(levelno)03d:%(levelname)-8s %(name)-12s"
    "%(module)s:%(lineno)s:%(funcName)s %(message)s",
)


class DebugGenerator(conans.model.Generator):
    def __init__(self, conanfile: conans.ConanFile):
        super(DebugGenerator, self).__init__(conanfile)
        self.conanfile = conanfile
        self.env = conanfile.env

    @property
    def filename(self) -> str:
        return "debug.txt"

    @property
    def content(self) -> str:

        content = []
        content.append("== env_info")
        content.append("{}".format(self.conanfile.env_info))

        content.append("== env_info.json")
        content.append(json.dumps(self.conanfile.env_info, sort_keys=True, indent=4))

        content.append("== env")
        content.append("{}".format(self.conanfile.env))
        logging.info("env = %s", self.conanfile.env)

        content.append("== env.json")
        content.append(json.dumps(self.conanfile.env, sort_keys=True, indent=4))

        return os.linesep.join(content)


class TheConanFile(conans.ConanFile):
    name = "debug-generator"
    version = "0.1.0"
    default_user = "xadix"
    default_channel = "default"
    build_policy = "missing"

    def build(self) -> None:
        pass

    def package_info(self) -> None:
        self.cpp_info.includedirs = []
        self.cpp_info.libdirs = []
        self.cpp_info.bindirs = []
