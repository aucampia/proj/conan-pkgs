# vim: set noexpandtab fo-=t:

# https://www.gnu.org/software/make/manual/make.html

########################################################################
# boiler plate
########################################################################

.PHONY: default
default:

SHELL=bash
current_makefile:=$(lastword $(MAKEFILE_LIST))
current_makefile_dirname:=$(dir $(current_makefile))
current_makefile_dirname_abspath:=$(dir $(abspath $(current_makefile)))
current_makefile_dirname_realpath:=$(dir $(realpath $(current_makefile)))

__empty:=
__space:=$(__empty) $(__empty)

define __newline


endef

ifneq ($(filter all vars,$(VERBOSE)),)
dump_var=$(info var $(1)=$($(1)))
dump_vars=$(foreach var,$(1),$(call dump_var,$(var)))
else
dump_var=
dump_vars=
endif

ifneq ($(filter all targets,$(VERBOSE)),)
__ORIGINAL_SHELL:=$(SHELL)
SHELL=$(warning Building $@$(if $<, (from $<))$(if $?, ($? newer)))$(TIME) $(__ORIGINAL_SHELL)
endif

$(call dump_vars,current_makefile current_makefile_dirname \
	current_makefile_dirname_abspath current_makefile_dirname_realpath)

########################################################################
# useful ...
########################################################################

## force ...
.PHONY: .FORCE
.FORCE:
$(force_targets): .FORCE

## dirs ...
.PRECIOUS: %/
%/:
	mkdir -vp $(@)

.PHONY: clean-%/ clean
clean-%/:
	{ test -d $(*) && rm -vr $(*); } || echo "$(@): $(*) does not exist ..."

## stamps
.PHONY: .FORCE
__refresh_stamps_target=$(if $(refresh_stamps),.FORCE,)

%.stamp: $(__refresh_stamps_target)
	echo stamp > $(@)

%.tstamp: $(__refresh_stamps_target)
	date +%Y-%m-%dT%H:%M:%S > $(@)

.PHONY: clean-stamp clean
clean: clean-stamp
clean-stamp:
	rm -v *.stamp *.tsstamp || true

## functions ...
reflex_path=$(subst $(__space),/,$(foreach dir,$(subst /,$(__space),$(1)),..))

true:=T
false:=
seq = $(if $(subst x$(1)x,,x$(2)x)$(subst x$(2)x,,x$(1)x),$(false),$(true))
assert=$(if $(call seq,$(1),$(2)),$(1),$(error expected output $(2) did not match actual output $(1)))

########################################################################
# local ...
########################################################################

########################################################################
# targets ...
########################################################################

clean_force=$(false)

.PHONY: nothing
nothing:
	@echo doing $(@) ...

export CONAN_VERBOSE_TRACEBACK=1
export CONAN_PRINT_RUN_COMMANDS=1

cpkgs=$(filter-out template boilerplate,$(notdir $(wildcard conan-pkg/*)))
$(info cpkgs=$(cpkgs))
$(info cpkgs=$(cpkgs:%=cpkg-clean-%))

## cpkg

.PHONY: cpkg-create cpkg-build cpkg-upload cpkg-clean cpkg-remove
cpkgs-create: $(cpkgs:%=cpkg-create-%)
cpkgs-build: $(cpkgs:%=cpkg-build-%)
cpkgs-upload: $(cpkgs:%=cpkg-upload-%)
cpkgs-uploadall: $(cpkgs:%=cpkg-uploadall-%)
cpkgs-clean: $(cpkgs:%=cpkg-clean-%)
cpkgs-remove: $(cpkgs:%=cpkg-remove-%)
cpkgs-inspect: $(cpkgs:%=cpkg-inspect-%)
cpkgs-install: $(cpkgs:%=cpkg-install-%)

cpkg_sets= \
	gens \
	java \

cpkg_set_gens = \
	debug-generator \
	gnumake-generator \

cpkg_set_java = \
	openjre-prebuilt \
	openjdk-prebuilt \
	gradle-prebuilt \
	groovy-prebuilt \
	graalvm-ce-prebuilt \
	kotlin-prebuilt \
	kscript-prebuilt \
	maven-prebuilt \
	scala-prebuilt \


define make_pkg_set
$$(call dump_vars,cpkg_set_$(1))
cpkgs-build-$(1): $$(cpkg_set_$(1):%=cpkg-build-%)
cpkgs-create-$(1): $$(cpkg_set_$(1):%=cpkg-create-%)
cpkgs-upload-$(1): $$(cpkg_set_$(1):%=cpkg-upload-%)
cpkgs-uploadall-$(1): $$(cpkg_set_$(1):%=cpkg-uploadall-%)
cpkgs-clean-$(1): $$(cpkg_set_$(1):%=cpkg-clean-%)
cpkgs-remove-$(1): $$(cpkg_set_$(1):%=cpkg-remove-%)
cpkgs-inspect-$(1): $$(cpkg_set_$(1):%=cpkg-inspect-%)
cpkgs-install-$(1): $$(cpkg_set_$(1):%=cpkg-install-%)
endef

#$(foreach cpkg_set,$(cpkg_sets),$(info $(call make_pkg_set,$(cpkg_set))))
$(foreach cpkg_set,$(cpkg_sets),$(eval $(call make_pkg_set,$(cpkg_set))))


CONAN_ARGS=
CONAN_CMD=conan
CONAN=CONAN_PRINT_RUN_COMMANDS=1 $(CONAN_CMD) $(CONAN_ARGS)
CONAN_CMD_ARGS=

conan_user=aucampia
conan_channel=default

#$(foreach cpkg,$(cpkgs),rsync -av --checksum common/xfce_tools.py conan-pkg/$(cpkg)$(__newline))
.PHONY: cpkg-include-common
cpkg-include-common:
	find conan-pkg/*/ -name 'ccf_*.lin' -printf '%h\n%P\n%l\n' \
		| sed -E 's/[.]lin$$//g' | tr '\n' '\000' \
		| xargs --no-run-if-empty -0 -n3 -t bash -c 'cp -fv $${1}/$${3} $${1}/$${2}' /dev/null

.PHONY: clean-cpkg-include-common
clean-cpkg-include-common:
	find conan-pkg/*/ -name 'ccf_*.lin' -printf '%h\n%P\n%l\n' \
		| sed -E 's/[.]lin$$//g' | tr '\n' '\000' \
		| xargs --no-run-if-empty -0 -n3 -t bash -c 'rm -v $${1}/$${2} || true' /dev/null

.PHONY: clean
clean: clean-cpkg-include-common


.PHONY: cpkg-create-%
cpkg-create-%: cpkg-include-common
	@echo making: $(@)
	$(CONAN) create --build=outdated --keep-source $(CONAN_CMD_ARGS) conan-pkg/$(*)/ $(conan_user)/$(conan_channel)

.PHONY: cpkg-createn-%
cpkg-createn-%: cpkg-include-common
	@echo making: $(@)
	$(CONAN) create --build=never --keep-source $(CONAN_CMD_ARGS) conan-pkg/$(*)/ $(conan_user)/$(conan_channel)

.PHONY: cpkg-export-%
cpkg-export-%: cpkg-include-common
	@echo making: $(@)
	$(CONAN) export --keep-source $(CONAN_CMD_ARGS) conan-pkg/$(*)/ $(conan_user)/$(conan_channel)

.PHONY: cpkg-info-%
cpkg-info-%: cpkg-include-common
	@echo making: $(@)
	$(CONAN) info $(CONAN_CMD_ARGS) conan-pkg/$(*)/

#.PHONY: cpkg-export-%
#cpkg-export-%: cpkg-include-common
#	@echo making: $(@)
#	$(CONAN) export --keep-source $(CONAN_CMD_ARGS) conan-pkg/$(*)/ $(conan_user)/$(conan_channel)

.PHONY: cpkg-remove-%
cpkg-remove-%:
	@echo making: $(@)
	$(CONAN) remove --force $(CONAN_CMD_ARGS) '$(*)/*'

.PHONY: cpkg-inspect-%
cpkg-inspect-%:
	@echo making: $(@)
	$(CONAN) inspect $(CONAN_CMD_ARGS) conan-pkg/$(*)/

conan_remote_args=--remote localhost

.PHONY: cpkg-upload-%
cpkg-upload-%: cpkg-export-%
	@echo making: $(@)
	$(CONAN) upload --confirm $(conan_remote_args) $(CONAN_CMD_ARGS) '$(*)/*@$(conan_user)/*'

.PHONY: cpkg-uploadall-%
cpkg-uploadall-%: cpkg-create-%
	@echo making: $(@)
	$(CONAN) upload --confirm --all $(conan_remote_args) $(CONAN_CMD_ARGS) '$(*)/*@$(conan_user)/*'

# https://docs.conan.io/en/latest/reference/commands/creator/create.html

.PHONY: cpkg-clean-%
cpkg-clean-%:
	@echo making: $(@)
	! test -e conan-pkg/$(*)/build || find conan-pkg/$(*)/build -name .git -print0 | xargs -t -0 --no-run-if-empty rm -vrf
	! test -e conan-pkg/$(*)/build || rm $(if $(clean_force),-f) -f -vr conan-pkg/$(*)/build

#.PHONY: cpkg-install-%
#cpkg-install-%: $(if $(clean_install),cpkg-clean-%,) cpkg-include-common
#	@echo making: $(@)
#	$(CONAN) install \
#		--build=outdated \
#		--install-folder=conan-pkg/$(*)/build \
#		$(CONAN_CMD_ARGS) $(CONAN_INSTALL_ARGS) \
#		conan-pkg/$(*)/

#		"$$(conan search '$(*)/*@$(conan_user)/*' | tail -1)"

.PHONY: cpkg-install-%
cpkg-install-%: $(if $(clean_install),cpkg-clean-%,) cpkg-export-% cpkg-include-common
	@echo making: $(@)
	$(CONAN) install \
		--build=outdated \
		--install-folder=conan-pkg/$(*)/build \
		$(CONAN_CMD_ARGS) $(CONAN_INSTALL_ARGS) \
		"$(*)/$$(conan inspect ./conan-pkg/python | python3 -c 'import sys, yaml, json; json.dump(yaml.safe_load(sys.stdin), sys.stdout, indent=2)' | jq -r '.version')@$(conan_user)/$(conan_channel)"

#cpkg-source-%: cpkg-install-%
.PHONY: cpkg-source-%
cpkg-source-%:
	@echo making: $(@)
	$(CONAN) source --install-folder=conan-pkg/$(*)/build --source-folder=conan-pkg/$(*)/build $(CONAN_CMD_ARGS) conan-pkg/$(*)/

.PHONY: cpkg-build-%
cpkg-build-%: cpkg-source-%
	@echo making: $(@)
	$(CONAN) build \
		--install-folder=conan-pkg/$(*)/build \
		--source-folder=conan-pkg/$(*)/build \
		--build-folder=conan-pkg/$(*)/build \
		$(CONAN_CMD_ARGS) $(CONAN_BUILD_ARGS) \
		conan-pkg/$(*)/

.PHONY: cpkg-package-%
cpkg-package-%: cpkg-build-%
	@echo making: $(@)
	$(CONAN) package --install-folder=conan-pkg/$(*)/build --source-folder=conan-pkg/$(*)/build --build-folder=conan-pkg/$(*)/build $(CONAN_CMD_ARGS) conan-pkg/$(*)/

## cdata

.PHONY: cdata-clean cdata-ls
cdata-clean: $(cpkgs:%=cdata-clean-%)
cdata-ls: $(cpkgs:%=cdata-ls-%)

.PHONY: cdata-clean-%
cdata-clean-%:
	! test -e ~/.conan/data/$(*) || rm -fvr ~/.conan/data/$(*)

.PHONY: cdata-ls-%
cdata-ls-%:
	! test -e ~/.conan/data/$(*)/*/*/*/package/* || ls -ld ~/.conan/data/$(*)/*/*/*/package/*

## cuse
cuses=$(notdir $(patsubst %/,%,$(dir $(wildcard conan-use/*/conanfile.txt))))
#cuses=$(notdir $(dir $(wildcard conan-use/*/conanfile.txt)))
$(info cuses=$(cuses))

.PHONY: cuse-install cuse-clean

cuse-install: $(cuses:%=cuse-install-%)
cuse-clean: $(cuses:%=cuse-clean-%)

.PHONY: cuse-install-%
cuse-install-%:
	$(CONAN) install --build=outdated --install-folder=conan-use/$(*)/build $(CONAN_CMD_ARGS) conan-use/$(*)/

.PHONY: cuse-clean-%
cuse-clean-%:
	! test -e conan-use/$(*)/build || rm -fvr conan-use/$(*)/build

## cx

.PHONY: cx-upload
cx-upload:
	$(CONAN) search -j /dev/stderr 2>&1 1>/dev/null | jq -r '.results[].items[].recipe.id' | tr '\n' '\000' | xargs -0 -n1 conan upload --confirm --all $(conan_remote_args)

## python management ...

.PHONY: update-latest
update-latest:
	dasel select -f pyproject.toml -m 'tool.poetry.dev-dependencies.-' \
		| sed 's/.*/&@latest/g' \
		| xargs -n1 poetry add --dev
	dasel select -f pyproject.toml -m 'tool.poetry.dependencies.-' \
		| grep -v '^python' \
		| sed 's/.*/&@latest/g' \
		| xargs -n1 poetry add

.PHONY: test
test:
	poetry run pytest --cov-config=.coveragerc --cov=src ./tests

.PHONY: validate-static
validate-static:
	poetry run isort --check $(py_source)
	poetry run black --check $(py_source)
	poetry run flake8 $(py_source)
	poetry run mypy --show-error-codes --show-error-context \
		$(py_source)


.PHONY: validate
validate: validate-static test
default: validate

.PHONY: validate-fix
validate-fix:
	poetry run isort $(py_source)
	poetry run black $(py_source)

################################################################################
## poddy ...
################################################################################

oci_name=Of8ohBeim2ooKooG
oci_cache_dir=$(HOME)/.cache/oci/centos-8-var-cache
oci_image=docker.io/centos:8
oci_image=docker.io/fedora:32

ospod-rm:
	podman container rm -fv $(oci_name)

ospod-run:
	mkdir -p "$(oci_cache_dir)"
	podman container run  -e DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix \
		--security-opt label=disable --privileged \
		--network=host \
		--volume "$(oci_cache_dir):/var/cache:z" \
		--volume "$(current_makefile_dirname_realpath):/var/tmp/workdir:z" \
		--workdir "/var/tmp/workdir" \
		--volume /etc/localtime:/etc/localtime:ro --detach --name $(oci_name) $(oci_image) \
		bash -c 'sed -E -i "s/^\s*metadata_expire\s*=.*$/metadata_expire=never/g" /etc/yum.repos.d/*; printf "metadata_expire=never\nkeepcache=True\nfastestmirror=True\n" >> /etc/dnf/dnf.conf; exec tail -F /dev/null'; true
	podman container exec -i $(oci_name) dnf install -y make which python3 python3-pip sudo findutils jq gcc perl cmake
	podman container exec -i $(oci_name) pip3 install --cache-dir=/var/cache/python3-pip/ --upgrade conan poetry dataclasses
	podman container exec -i $(oci_name) conan profile new --detect --force default
	podman container exec -i $(oci_name) conan remote add localhost http://localhost:9300/
	podman container exec -i $(oci_name) conan user --remote localhost --password demo demo


ospod-sh:
	podman container exec -it $(oci_name) bash


################################################################################
## clean
################################################################################

clean:
	find conan-pkg/*/build -name .git -print0 | xargs -t -0 --no-run-if-empty rm -vrf
	rm -vrf conan-pkg/*/build/; true
	rm -vrf conan-use/*/build/; true
	rm -vrf conan-use/*/build-conan/; true
	rm -vrf conan-pkg/*/build-conan/; true
